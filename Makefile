GENFILES=$(filter-out src/gen/., $(filter-out src/gen/.., $(wildcard src/gen/.* src/gen/*)))
WORKFILES=$(filter-out src/work/., $(filter-out src/work/.., $(wildcard src/work/.* src/work/*)))
HOMEFILES=$(filter-out src/home/., $(filter-out src/home/.., $(wildcard src/home/.* src/home/*)))

all: home

gen: $(GENFILES)
	cp -R $(GENFILES) ~

work: gen
	cp -R $(WORKFILES) ~

home: gen
	cp -R $(HOMEFILES) ~
